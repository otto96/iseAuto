#!/bin/bash
#SBATCH -N 6
#SBATCH -n 120
#SBATCH --ntasks-per-node=20
#SBATCH -t 02:00:00
#SBATCH --partition=testing
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=otto.joeleht@ut.ee

module purge
module load qt-4.8.6 
module load openmpi-2.1.0
module load zlib-1.2.6
module load cmake-2.8.12.2

cd $HOME
cd OpenFOAM8
cd OpenFOAM-4.x
export MPI_ARCH_PATH=/storage/software/openmpi-2.1.0/
export WM_COMPILE_OPTION=-I/storage/software/openmpi-2.1.0/include
export FOAM_INST_DIR=$HOME/OpenFOAM8
export WM_CC=mpicc; export WM_CXX=mpicxx
export WM_COMPILE_OPTION=-I$HOME/OpenFOAM8/flex/install/include
foamDotFile=$FOAM_INST_DIR/OpenFOAM-4.x/etc/bashrc
[ -f $foamDotFile ] && . $foamDotFile
source etc/bashrc WM_NCOMPPROCS=1024 WM_MPLIB=SYSTEMOPENMPI
export PATH=$PATH:$HOME/OpenFOAM8/flex/install/bin
export C_INCLUDE_PATH=/storage/software/openmpi-2.1.0/include:$HOME/OpenFOAM8/flex/install/include
export CPLUS_INCLUDE_PATH=/storage/software/openmpi-2.1.0/include:$HOME/OpenFOAM8/flex/install/include
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib64:$HOME/OpenFOAM8/gmp/install/lib:$HOME/OpenFOAM8/flex/install/lib
export LIBRARY_PATH=/storage/software/zlib-1.2.6/lib:$HOME/OpenFOAM8/gmp/install/lib:$HOME/OpenFOAM8/flex/install/lib
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/storage/software/zlib-1.2.6/include:$HOME/OpenFOAM8/gmp/install/include
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/storage/software/zlib-1.2.6/include:$HOME/OpenFOAM8/gmp/install

cd $HOME
cd OPENFOAMtest
cd iseAutoKLASTER
./Allrun
